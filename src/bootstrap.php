<?php

require 'src/Database/Connector.php';
require 'src/Database/QueryBuilder.php';


return new QueryBuilder(Connector::make());
